# My personal docker space

This repo is the configuration to run every docker container that I need for development or project specific development environment.

The `docker-compose.yml` load the different containers and the `.env` is used to change the configuration as much as possible to avoid writing directly in the `docker-compose.yml`. Each container has a seperate environment for all config (requirements, config file, some script, Dockerfile).

Check the [board](https://gricad-gitlab.univ-grenoble-alpes.fr/reusm/docker_config/-/boards/2847) for the future road map of the Configurations.

## `.env` file

The `.env` file has a set of env variable which are loaded to the running container. This section is a small description of each description.

- `USERNAME` is the container user. It should be the same as the local user when the container edits file from a container.
- `UID` is the container user ID. It should be the same as the local user when the container edits file from a container.
- `GID` is the container group user ID. It should be the same as the local user when the container edits file from a container.
- `JUPYTER_TOKEN` is the simple token use to keep the same *password* for each jupyter server.
- `TORCH_HOME` is an environment variable to locate the torch cache in a specific folder.
- `RAM` is the quantity of RAM allowed by the container.
- `TORCH_VERSION` is the version of torch to take in the image (by default)
- `CUDA` is the version of CUDA to take in the image (by default)
- `CUDNN` is the version of CUDNN to take in the image (by default)
- `PSTR_CODE_DIRA` is the path of the mounted volume of PSTR repo
- `CU_VERSION` is the version of CUDNN to take in MMCV install, see [here](https://mmcv.readthedocs.io/en/latest/get_started/installation.html) for more info

## Docker envs

This section is a quick explanation of the docker env to keep track of what is going on.

### General development for personal usage

This environment have `torch` as base image. It uses ressources for graphic card to run.

It has access to a VS-Code directory in order to attach it with VS-Code for devs. Also, you have acces to the code directory: `~/code/`.

It runs a jupyter server for convenience of development (which is directly installed in the conda env) and some additionnal package (installed with pip).

The directories have two file :

- `Dockerfile` configures the user, the environment and required packages. Then it runs the jupyter server.
- `requirements.txt` for installing package using pip.

### PSTR

This is the dockerfile from [PSTR](https://github.com/JialeCao001/PSTR), I added some lines for the permissions and run the jupyter server on it. 

Basically I install the `mmcv-full` package with the correct CUDNN/CUDA version from MMLAB website and install the PSTR repo on it.


