# Script to build docker image with docker-compose-minext.yml 
# and the Dockerfile of MinExt
# $1 takes a set of values which is mapped to a building stage:
# - "dev" for DEV_ENV 
# - "train" for TRAIN
# $2 takes build or up as values
#
# I learn how to do this from a blog
# https://ostechnix.com/bash-associative-array/

# Dictionnary to map args and building stages
declare -A serviceNameByArgs=(
  ["dev"]="minext-dev"
  ["train"]="minext-train"
  ["down"]="down containers"
)

dockerComposeWithFileArg="docker-compose -f docker-compose-minext.yml"

if [[ -n "${serviceNameByArgs[$1]}" ]]
then
  if [ $1 == "down" ]
  then
    $dockerComposeWithFileArg down
    exit 0
  fi

  case $2 in # Build or up the containers
    build) 
    ${dockerComposeWithFileArg} build ${serviceNameByArgs[$1]};;
    up)
    ${dockerComposeWithFileArg} up -d ${serviceNameByArgs[$1]};;
    bash)
    ${dockerComposeWithFileArg} exec ${serviceNameByArgs[$1]} bash;;
    up-bash)
    ${dockerComposeWithFileArg} up -d ${serviceNameByArgs[$1]}
    ${dockerComposeWithFileArg} exec ${serviceNameByArgs[$1]} bash;;
    build-up-bash)
    ${dockerComposeWithFileArg} build ${serviceNameByArgs[$1]}
    ${dockerComposeWithFileArg} up -d ${serviceNameByArgs[$1]}
    ${dockerComposeWithFileArg} exec ${serviceNameByArgs[$1]} bash;;
    *)
    echo "docker-compose sub-command $2 is not supported"
    echo "Only subcommand down, up, bash, up-bash and build-up-bash are supported";;
  esac
else
  echo "$1 is not a valid arg"
  echo "Valid args are ${!serviceNameByArgs[@]}"
fi 
